package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GitRepository {
	private String repoPath;
	
	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}
	
	private Scanner getFileScanner(String relativePath) throws FileNotFoundException {
		FileInputStream stream = new FileInputStream(repoPath + "/" + relativePath);
		return new Scanner(stream);
	}

	public String getHeadRef() throws FileNotFoundException {
		Scanner s = getFileScanner("HEAD");
		String ref = s.nextLine().split(" ")[1];
		s.close();
		return ref;
	}

	public String getRefHash(String ref) throws FileNotFoundException {
		Scanner s = getFileScanner(ref);
		String hash = s.nextLine();
		s.close();
		return hash;
	}
}
